class App {
  init() {
    this.render();
  }
  render() {
    let wrapper = document.getElementById("app");
    // wrapper.style.backgroundColor = 'red';
  }
}

class LoginForm {
  constructor(parent, id) {
    //Our form's wrapper
    this.parent = parent;
    this.id = id;

    //Our form
    this.element = document.createElement("form");
    this.element.id = this.id;

    // Declaring our form's input field, in our case : username & password
    this.username = new Input(
      this.element.id,
      "username-input",
      "Username",
      "text"
    );
    this.password = new Input(
      this.element.id,
      "password-input",
      "Password",
      "password"
    );

    // Declaring your submit button in order to trigger the handleSubmit event listener for your form
    this.submitElement = document.createElement("button");
    this.submitElement.id = this.id + "-button";
    this.submitElement.innerHTML = "Send";
    // see password value
    this.togglepassword = document.createElement("button");
    this.togglepassword.id = this.id + "-toggle";
    this.togglepassword.innerHTML = "voir";

    //Biding `this` (LoginForm class) to our this.handleSubmit method to access `this.username` for example
    this.handleSubmit = this.handleSubmit.bind(this);
    this.seePassword = this.seePassword.bind(this);
    //Adding a submit event listener yo submit our data
    this.element.addEventListener("submit", this.handleSubmit);
    this.togglepassword.addEventListener("click", this.seePassword);
  }

  init() {
    this.render();
    // if (this.togglepassword.eve) {
    //   this.render();
    // }
  }

  handleSubmit(e) {
    e.preventDefault(); // не дает отправить раньше чем внесены дынне в форму

    let body = {
      username: this.username.value,
      password: this.password.value
    };

    fetch("http://localhost:3000/api/users/authenticate", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body) // body data type must match "Content-Type" header
    }).then(response => {
      response.json().then(data => {
        console.log(data);
      });
    });
  }
  seePassword() {
    console.log(this.password.type);
    if (this.password.type === "password") {
      this.password.type = "text";
    } else {
      this.password.type = "password";
    }
  }

  render() {
    let wrapper = document.getElementById(this.parent);
    wrapper.append(this.element);

    //We're initiating our Input components
    this.username.init();
    this.password.init();
    this.element.appendChild(this.togglepassword);
    //We're finally calling our submit button as the last child of our form
    this.element.append(this.submitElement);
  }
}

class Input {
  constructor(parent, id, placeholder, type) {
    this.parent = parent;
    this.id = id;
    this.placeholder = placeholder;
    this.type = type;

    this.element = document.createElement("input");
    this.element.id = this.id;
    // this.element.value = this.value;
    this.element.placeholder = this.placeholder;
    this.element.type = this.type;

    this.handleChange = this.handleChange.bind(this);
    this.element.addEventListener("change", this.handleChange);
  }
  init() {
    this.render();
  }
  handleChange(e) {
    e.preventDefault();
    this.value = e.target.value;
  }
  render() {
    // Input's parent
    let wrapper = document.getElementById(this.parent);
    wrapper.appendChild(this.element);
  }
}

let AppComponent = new App();
let FormComponent = new LoginForm("app", "form");

AppComponent.init();
FormComponent.init();
