import express from "express";

import projectRoutes from "../modules/modules/project/routes";
import pageRoutes from "../modules/modules/page/routes";
import userRoutes from "../modules/modules/user/routes";
// const path = require("path"); comme l'exemple
// const option = { root: path.join("../front/src/views/") };

const Router = app => {
  var apiRoutes = express.Router();

  // Home route. We'll end up changing this to our main front end index later.
  app.get("/", function(req, res) {
    res.send("This Route is not yet defined.");
  });
  // app.get("/", function(req, res) { comme l'exemple
  //   res.sendFile("index.html", option);
  // });

  //Project router
  app.use("/api/", apiRoutes);

  app.use("/api/project", projectRoutes);
  app.use("/api/page", pageRoutes);
  app.use("/api/user", userRoutes);
};

export default Router;
