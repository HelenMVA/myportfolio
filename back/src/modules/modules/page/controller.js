import PageServices from "./service";

const PageController = {
  allPage: (req, res) => {
    // console.log(req);
    // res.send(200, "All projects: []");
    PageServices.allPage(req, result => {
      console.log(result);
      // res.send(200, result);
      //   res.status(200).send(result); example
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
  keyPage: (req, res) => {
    PageServices.keyPage(req.params.id, result => {
      console.log(result);
      // res.send(200, result);
      //   res.status(200).send(result); example
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  }
};
export default PageController;
