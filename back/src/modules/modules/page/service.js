import query from "../page/query";

import { response } from "express";
const PageServices = {
  allPage: (req, callback) => {
    query.getAllPage(
      req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      }
    );
  },
  keyPage: (id, callback) => {
    query.getkeyPage(
      id,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      }
    );
  }
};

export default PageServices;
