import connection from "../../../setup/database";

// Notre query s'occupe d'effectuer la requête sur la base de donneés et de renvoyer au service les datas
const Query = {
  getAllPage: (param, successCallback, failureCallback) => {
    let sqlQuery = "SELECT * FROM `page`";
    // let sqlQuery = "SELECT * FROM `test`";

    connection.query(sqlQuery, (err, rows, fields, res) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return failureCallback("No projects.");
      }
    });
  },
  getkeyPage: (id, successCallback, failureCallback) => {
    // console.log(param.params.id);
    let sqlQuery = `SELECT * FROM page WHERE id_page=${id}`;
    // let sqlQuery = "SELECT * FROM `test`";

    connection.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return failureCallback("No projects.");
      }
    });
  }
};

export default Query;
