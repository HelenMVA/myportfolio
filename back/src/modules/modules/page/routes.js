import express from "express";
import PageController from "./controller";
const router = express.Router();

// router.get("/", (req, res) => {
//   res.send(200, "Super");
// });

//http://localhost:3000/api/

router.get("/all", PageController.allPage);
// router.get("/key/:id", PageController.keyPage);
router.get("/:id", PageController.keyPage);
export default router;
