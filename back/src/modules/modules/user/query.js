import db from "../../../setup/database";

// Notre query s'occupe d'effectuer la requête sur la base de donneés et de renvoyer au service les datas
const Queries = {
  getAll: (param, successCallback, failureCallback) => {
    let sqlQuery = "SELECT * FROM `user`";

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No users.");
      }
    });
  },
  getById: (id, successCallback, failureCallback) => {
    let sqlQuery = `SELECT * FROM user WHERE ID=${id}`;

    db.query(sqlQuery, (err, rows) => {
      if (err) {
        return failureCallback(err);
      }
      if (rows.length > 0) {
        return successCallback(rows);
      } else {
        return successCallback("No matching user");
      }
    });
  },
  // authenticate: (user, successCallback, failureCallback) => {
  //   let sqlQuery = `SELECT * FROM USER WHERE name="${user.name}" AND password="${user.password}"`;

  //   db.query(sqlQuery, (err, rows) => {
  //     if (err) {
  //       return failureCallback(err);
  //     }
  //     if (rows.length > 0) {
  //       return successCallback(rows[0]);
  //     } else {
  //       return successCallback("Incorrect username or password combinaison");
  //     }
  //   });
  // },
  getByUsername: username => {
    let sqlQuery = `SELECT * FROM user WHERE name="${username}"`;

    return new Promise((resolve, reject) => {
      db.query(sqlQuery, (err, rows) => {
        if (err) reject(err);
        resolve(rows[0]);
      });
    });
  },
  register: async user => {
    return new Promise((resolve, reject) => {
      let sqlQuery = `INSERT INTO user (id, name, password, email, name_role) VALUES (NULL,"${user.name}","${user.hashedPassword}", "${user.email}", "user" )`;

      db.query(sqlQuery, (err, res) => {
        if (err) reject(err);
        resolve(res);
      });
    });
  }
};

export default Queries;
