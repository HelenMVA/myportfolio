import UserQueries from "./query";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import config from "../../../config/server.json";

const UserServices = {
  getAll: (req, callback) => {
    UserQueries.getAll(
      req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      }
    );
  },
  getById: (id, callback) => {
    UserQueries.getById(
      id,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      }
    );
  },
  authenticate: async body => {
    let { name, password } = body;

    if (typeof name !== "string" || typeof password !== "string") {
      return {
        status: 400,
        payload: {
          success: false,
          message: "All fields are required and must be a string type"
        }
      };
    }

    const user = await UserQueries.getByUsername(name);

    if (!user) {
      return {
        status: 403,
        payload: { success: false, message: "Username not found" }
      };
    }

    const passwordMatched = await bcrypt.compare(password, user.password);

    if (passwordMatched) {
      const token = jwt.sign(
        { id: user.id, role: user.name_role },
        config.secret
      );
      const { password, ...userWithoutPassword } = user;
      return {
        status: 200,
        payload: {
          success: true,
          message: "User correctly authenticated",
          data: { token: token, user: { ...userWithoutPassword } }
        }
      };
    }

    return {
      status: 403,
      payload: { success: false, message: "Username & password missmatch" }
    };
  },
  register: async body => {
    let { email, password, name } = body;

    if (
      typeof email !== "string" ||
      typeof password !== "string" ||
      typeof name !== "string"
    ) {
      return {
        status: 400,
        payload: {
          success: false,
          message: "All fields are required and must be a string type"
        }
      };
    }

    return bcrypt
      .genSalt()
      .then(salt => bcrypt.hash(password, salt))
      .then(hashedPassword =>
        UserQueries.register({ email, hashedPassword, name })
      )
      .then(user => ({
        status: 201,
        payload: { success: true, message: "User successfully registered" }
      }))
      .catch(err => ({
        status: 400,
        payload: { success: false, message: err }
      }));
  }
};

export default UserServices;
