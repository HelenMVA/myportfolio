import ProjectServices from "./service";

const ProjectController = {
  allProjects: (req, res) => {
    // console.log(req);
    // res.send(200, "All projects: []");
    ProjectServices.allProjects(req, result => {
      console.log(result);
      // res.send(200, result);
      //   res.status(200).send(result); example
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  }
};
export default ProjectController;
