import express from "express";
import ProjectController from "./controller";
const router = express.Router();

// router.get("/", (req, res) => {
//   res.send(200, "Super");
// });

router.get("/all", ProjectController.allProjects);
export default router;
