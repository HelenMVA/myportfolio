import query from "../project/query";

import { response } from "express";
const ProjectServices = {
  allProjects: (req, callback) => {
    query.getAllProjects(
      req,
      response => {
        return callback({ success: true, message: response });
      },
      error => {
        return callback({ success: false, message: error });
      }
    );
  }
};

export default ProjectServices;
